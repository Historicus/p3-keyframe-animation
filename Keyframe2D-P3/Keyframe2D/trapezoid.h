/**
 * Header for Trapezoid subclass of Geometry
 *
 * Samuel Kenney
 * Grove City College
 * March 17, 2017
 */

#pragma once
#include "Geometry.h"
#include "gVector4.h"

class trapezoid : public Geometry
{
public:
	trapezoid(unsigned int);
	~trapezoid(void);
	void draw(const gMatrix4&);

private:
	gVector4* points;
	gVector4* colors;
	unsigned int pointCount;
};

