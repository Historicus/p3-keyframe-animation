/**
 * cpp file for SceneGraph class
 *
 * Samuel Kenney
 * Grove City College
 * March 24, 2017: Updated April 3rd, 2017
 *
 * Each node will have the same size frameData and keyFrameNumbers vectors. But the struct information for each will be different in each one
 *
 */

#include "SceneGraph.h"

//initialize nodeCount to be 0, then increment in constructor
int SceneGraph::nodeCount = 0;

//Standard constructor, should create inert transformation variables
//If the node shouldn't render anything, pass a nullptr as the parameter.
SceneGraph::SceneGraph(Geometry* node){
	children.clear();
	geom = node;

	//create transform
	transforms start;
	start.theta = 0.0f, start.scaleFactor[0] = 1.0f, start.scaleFactor[1] = 1.0f, start.translation[0] = 0.0f, start.translation[1] = 0.0f;
	//initial struct for start keyframe
	frameData.push_back(start);
	//initial struct for final keyframe
	//frameData.push_back(start);

	//initial keyframe
	keyframeNumbers.push_back(0);
	//final keyframe
	//keyframeNumbers.push_back(1);

	nodeID = nodeCount;
	nodeCount = nodeCount + 1;

	recentKeyIndex = 0;

	currentKeyIndex = 0;
}

//Copy constructor, since this is a tree/graph, you need to perform a deep copy
//The default copy constructor only does a shallow copy
SceneGraph::SceneGraph(const SceneGraph& sg){
	geom = sg.geom;

	keyframeNumbers = sg.keyframeNumbers;
	frameData = sg.frameData;
	
	children = sg.children;

	recentKeyIndex = sg.recentKeyIndex;

	nodeCount = sg.nodeCount;
	nodeID = sg.nodeID;

}

//destructor
SceneGraph::~SceneGraph(void){
}

//Add the new scene graph node to the end of the children list
void SceneGraph::addChild(const SceneGraph& node){
	children.push_back(node);
}

//traverse the scene graph as discussed in class
//Old traverse, should only take frameData[0]
void SceneGraph::traverse(gMatrix4 T) const{

	recentKeyIndex = 0;
	//1. "Do what I need to do"
	//	a. Multiply my transformations with the passed in matrix
	gMatrix4 scaleT = gMatrix4::scale3D(getScaleX(0), getScaleY(0), 1.0f);

	gMatrix4 rotateZ = gMatrix4::rotateZ(frameData[0].theta);

	gMatrix4 transT = gMatrix4::translate3D(getTranslateX(0), getTranslateY(0), 0.0f);

	gMatrix4 newT =	T * (transT * rotateZ * scaleT );

	//	b. Draw geometry if present, i.e., if ptr != nullptr
	if (geom != nullptr){
		geom->draw(newT);
	}

	//2. "Recurse over my children"
	std::list<SceneGraph>::const_iterator iterator;
	for (iterator = children.begin(); iterator != children.end(); ++iterator) {
		//traverse each child
		iterator->traverse(newT);
	}

}

//must pass in the frame number for animation
//keyframes should not be interpolated, and should cause recentKeyIndex to increment
//other frames should be interpolated by using frameData[recentKeyIndex] and frameData[recentKeyIndex+1]
void SceneGraph::traverse(gMatrix4 T, int frameNum) const{
	transforms interpolated;
	gMatrix4 newT;
	currentKeyIndex = recentKeyIndex;
	int frametoDraw = 0;
	//if not a keyframe
	if (std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum) == keyframeNumbers.end()){ 
		if (frameNum < keyframeNumbers[recentKeyIndex]){
			recentKeyIndex--;
		}
		//interpolate
		float delta = frameNum - keyframeNumbers[recentKeyIndex];
		float d = keyframeNumbers[recentKeyIndex+1] - keyframeNumbers[recentKeyIndex];
		float t = delta / d;

		//(1-t)variablex - (t)variablex+1
		//these are the right equations but it does not interpolate correctly yet
		interpolated.theta = ((1.0f-t)*(frameData[recentKeyIndex].theta) + (t)*(frameData[recentKeyIndex+1].theta));
		interpolated.scaleFactor[0] = ((1.0f-t)*(frameData[recentKeyIndex].scaleFactor[0]) + (t)*(frameData[recentKeyIndex+1].scaleFactor[0]));
		interpolated.scaleFactor[1] = ((1.0f-t)*(frameData[recentKeyIndex].scaleFactor[1]) + (t)*(frameData[recentKeyIndex+1].scaleFactor[1]));
		interpolated.translation[0] = ((1.0f-t)*(frameData[recentKeyIndex].translation[0]) + (t)*(frameData[recentKeyIndex+1].translation[0]));
		interpolated.translation[1] = ((1.0f-t)*(frameData[recentKeyIndex].translation[1]) + (t)*(frameData[recentKeyIndex+1].translation[1]));

		gMatrix4 scaleT = gMatrix4::scale3D(interpolated.scaleFactor[0], interpolated.scaleFactor[1], 1.0f);

		gMatrix4 rotateZ = gMatrix4::rotateZ(interpolated.theta);

		gMatrix4 transT = gMatrix4::translate3D(interpolated.translation[0], interpolated.translation[1], 0.0f);

		newT =	T * (transT * rotateZ * scaleT );

	} else {

		std::vector<int>::const_iterator it;
		it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

		int pos = std::distance(keyframeNumbers.begin(), it);

		//if it has to repaint it will not increment again but keep the same frame
		if (currentKeyIndex == pos){
			frametoDraw = pos;
		} else if (frameNum < keyframeNumbers[recentKeyIndex]){
			recentKeyIndex--;
			frametoDraw = recentKeyIndex;
		}else {
			recentKeyIndex++;
			frametoDraw = recentKeyIndex;
		}

		gMatrix4 scaleT = gMatrix4::scale3D(frameData[frametoDraw].scaleFactor[0], frameData[frametoDraw].scaleFactor[1], 1.0f);

		gMatrix4 rotateZ = gMatrix4::rotateZ(frameData[frametoDraw].theta);

		gMatrix4 transT = gMatrix4::translate3D(frameData[frametoDraw].translation[0], frameData[frametoDraw].translation[1], 0.0f);

		newT =	T * (transT * rotateZ * scaleT );
	}
	//	b. Draw geometry if present, i.e., if ptr != nullptr
	if (geom != nullptr){
		geom->draw(newT);
	}

	////2. "Recurse over my children"
	std::list<SceneGraph>::const_iterator iterator;
	for (iterator = children.begin(); iterator != children.end(); ++iterator) {
		//traverse each child
		iterator->traverse(newT, frameNum);
	}
}

//add keyframe
//recursive, create an interpolated struct, add it to frameData at each node, and add the keyFrameNumber
void SceneGraph::addKeyframe(int frameNum){

	//adds to the keyframeNumbers and then sorts----for some reason it can add the keyframe but it can't go back to it
	keyframeNumbers.push_back(frameNum);
	std::sort(keyframeNumbers.begin(), keyframeNumbers.end());

	//put in new frame num, find where it is, and return the index. That is the index you need for frameData
	std::vector<int>::iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of where the new struct needs to go
	//there will always be one before and one after for interpolation
	int pos = std::distance(keyframeNumbers.begin(), it);

	//interpolate
	float delta = frameNum - keyframeNumbers[pos-1];
	float d = keyframeNumbers[pos+1] - keyframeNumbers[pos-1];
	float t = delta / d;

	//(1-t)variablex - (t)variablex+1
	//these are the right equations but it does not interpolate correctly yet
	transforms interpolated;
	interpolated.theta = ((1.0f-t)*(frameData[pos-1].theta) + (t)*(frameData[pos].theta));
	interpolated.scaleFactor[0] = ((1.0f-t)*(frameData[pos-1].scaleFactor[0]) + (t)*(frameData[pos].scaleFactor[0]));
	interpolated.scaleFactor[1] = ((1.0f-t)*(frameData[pos-1].scaleFactor[1]) + (t)*(frameData[pos].scaleFactor[1]));
	interpolated.translation[0] = ((1.0f-t)*(frameData[pos-1].translation[0]) + (t)*(frameData[pos].translation[0]));
	interpolated.translation[1] = ((1.0f-t)*(frameData[pos-1].translation[1]) + (t)*(frameData[pos].translation[1]));

	//puts new keyframe with interpolated struct right between the two keyframes it needs to be between-- why would vector subscript be out of range if I add it right here?????
	frameData.emplace(frameData.begin()+pos, interpolated);

	std::list<SceneGraph>::iterator iterator;
	for (iterator = children.begin(); iterator != children.end(); ++iterator) {
		//traverse each child
		iterator->addKeyframe(frameNum);
	}
}

//remove keyframe
//recursive, figure out which frame data is relevant and remove it from each node, delete the keyFrameNumber
void SceneGraph::removeKeyframe(int frameNum){
	//find index of keyframeNumber
	std::vector<int>::iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	keyframeNumbers.erase(keyframeNumbers.begin()+pos);
	frameData.erase(frameData.begin()+pos);

	std::list<SceneGraph>::iterator iterator;
	for (iterator = children.begin(); iterator != children.end(); ++iterator) {
		//traverse each child
		iterator->removeKeyframe(frameNum);
	}
}

//reset keyframes - give max frame number
//recursive, remove all but the first and last elements of frameData, copy the first element's contents into the last element's contents
//clear the keyFrameNumbers and reinsert 0 and the passed in parameter
void SceneGraph::resetKeyframes(int frameNum){
	transforms temp = frameData[0];
	//remove all but the first element
	frameData.clear(); //Not sure why this is not resetting the values and making it work, does not make sense
	//copy first element into last element
	frameData.push_back(temp);
	frameData.push_back(temp);

	//clear keyframeNumbers
	keyframeNumbers.clear();
	//insert first and last to keyframeNumbers
	keyframeNumbers.push_back(0);
	keyframeNumbers.push_back(frameNum);

	recentKeyIndex = 0;

	std::list<SceneGraph>::iterator iterator;
	for (iterator = children.begin(); iterator != children.end(); ++iterator) {
		//traverse each child
		iterator->resetKeyframes(frameNum);
	}
}

//do a depth-first search of the SceneGraph to find the node with the corresponding ID, return a pointer to that node
SceneGraph* SceneGraph::findByID(int nodeID){
	//check if the current node is the correct node
	if (this->nodeID == nodeID){
		return this;
	} else {
		//create temp node
		SceneGraph* temp;
		//should be impossible to find a node that does not exist
		std::list<SceneGraph>::iterator iterator;
		for (iterator = children.begin(); iterator != children.end(); ++iterator){
			temp = iterator->findByID(nodeID);
			if (temp != nullptr){
				return temp;
			}
		}
		return nullptr;
	}
}

//setters and getters
float SceneGraph::getScaleX(int frameNum) const{
	//find index of keyframeNumber
	std::vector<int>::const_iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	return frameData[pos].scaleFactor[0];	
}
void SceneGraph::setScaleX(int frameNum,float scaleX){
	//find index of keyframeNumber
	std::vector<int>::iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	frameData[pos].scaleFactor[0] = scaleX;
}

float SceneGraph::getScaleY(int frameNum) const{
	//find index of keyframeNumber
	std::vector<int>::const_iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	return frameData[pos].scaleFactor[1];
}
void SceneGraph::setScaleY(int frameNum,float scaleY){
	//find index of keyframeNumber
	std::vector<int>::iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	frameData[pos].scaleFactor[1] = scaleY;
}

float SceneGraph::getTheta(int frameNum) const{
	//find index of keyframeNumber
	std::vector<int>::const_iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	return frameData[pos].theta;
}
void SceneGraph::setTheta(int frameNum,float angle){
	//find index of keyframeNumber
	std::vector<int>::iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	frameData[pos].theta = angle;
}

float SceneGraph::getTranslateX(int frameNum) const{
	//find index of keyframeNumber
	std::vector<int>::const_iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	return frameData[pos].translation[0];
}
void SceneGraph::setTranslateX(int frameNum,float transX){
	//find index of keyframeNumber
	std::vector<int>::iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	frameData[pos].translation[0] = transX;
}

float SceneGraph::getTranslateY(int frameNum) const{//find index of keyframeNumber
	std::vector<int>::const_iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	return frameData[pos].translation[1];
}
void SceneGraph::setTranslateY(int frameNum,float transY){
	//find index of keyframeNumber
	std::vector<int>::iterator it;
	it = std::find(keyframeNumbers.begin(), keyframeNumbers.end(), frameNum);

	//index of frameNum
	int pos = std::distance(keyframeNumbers.begin(), it);

	frameData[pos].translation[1] = transY;
}
