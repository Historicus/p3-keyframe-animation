#pragma once

#include <QSlider>

class FrameSlider : public QSlider {
	Q_OBJECT

public:
	FrameSlider(QWidget* parent = nullptr);
	~FrameSlider();

public slots:
	void setFrameCount(int);

private:
	
};
