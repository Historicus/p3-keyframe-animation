#pragma once

#include <QTreeWidget>

class GraphManager : public QTreeWidget
{
	Q_OBJECT

public:
	GraphManager(QWidget* parent = nullptr);
	~GraphManager();

public slots:
	void changeGeometrySelection(int);
	void addNewNode(void);
	void changeNodeSelection(QTreeWidgetItem*, QTreeWidgetItem*);

signals:
	void sendSelectedNode(int);
	void addNode(int);

private:
	int currentGeometrySelection;
};
