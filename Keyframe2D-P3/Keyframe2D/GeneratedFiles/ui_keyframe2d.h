/********************************************************************************
** Form generated from reading UI file 'keyframe2d.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYFRAME2D_H
#define UI_KEYFRAME2D_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>
#include "framecountselector.h"
#include "frameslider.h"
#include "graphmanager.h"
#include "intfloatspinbox.h"
#include "my_openglwidget.h"

QT_BEGIN_NAMESPACE

class Ui_Keyframe2DClass
{
public:
    QAction *actionExit;
    QWidget *centralWidget;
    My_OpenGLWidget *openGLWidget;
    QFrame *line;
    QFrame *line_2;
    GraphManager *treeWidget;
    QPushButton *addChildButton;
    QComboBox *geometryComboBox;
    QFrame *line_3;
    QGroupBox *groupBox;
    QSlider *txSlider;
    QSlider *tySlider;
    QLabel *label;
    QLabel *label_3;
    IntFloatSpinBox *txSpinner;
    IntFloatSpinBox *tySpinner;
    QGroupBox *groupBox_2;
    QSlider *sxSlider;
    QSlider *sySlider;
    QLabel *label_4;
    QLabel *label_5;
    IntFloatSpinBox *sySpinner;
    IntFloatSpinBox *sxSpinner;
    QGroupBox *groupBox_4;
    QSpinBox *thetaSpinner;
    QSlider *theta_slider;
    QLabel *label_6;
    QGroupBox *groupBox_3;
    FrameSlider *frameSelection;
    QCheckBox *isKeyframeCheckBox;
    QLCDNumber *frameID;
    FrameCountSelector *frameSpinner;
    QPushButton *switchAnimateButton;
    QLabel *label_2;
    QPushButton *clearButton;
    QPushButton *resetButton;
    QPushButton *playButton;
    QPushButton *stopButton;

    void setupUi(QMainWindow *Keyframe2DClass)
    {
        if (Keyframe2DClass->objectName().isEmpty())
            Keyframe2DClass->setObjectName(QStringLiteral("Keyframe2DClass"));
        Keyframe2DClass->resize(891, 721);
        actionExit = new QAction(Keyframe2DClass);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(Keyframe2DClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        openGLWidget = new My_OpenGLWidget(centralWidget);
        openGLWidget->setObjectName(QStringLiteral("openGLWidget"));
        openGLWidget->setGeometry(QRect(9, 9, 550, 550));
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(0, 560, 881, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(560, 10, 20, 551));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        treeWidget = new GraphManager(centralWidget);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->setGeometry(QRect(580, 10, 301, 251));
        addChildButton = new QPushButton(centralWidget);
        addChildButton->setObjectName(QStringLiteral("addChildButton"));
        addChildButton->setGeometry(QRect(580, 270, 71, 31));
        geometryComboBox = new QComboBox(centralWidget);
        geometryComboBox->setObjectName(QStringLiteral("geometryComboBox"));
        geometryComboBox->setGeometry(QRect(760, 270, 121, 31));
        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(580, 300, 301, 16));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(580, 320, 301, 81));
        txSlider = new QSlider(groupBox);
        txSlider->setObjectName(QStringLiteral("txSlider"));
        txSlider->setGeometry(QRect(40, 20, 191, 22));
        txSlider->setMinimum(-150);
        txSlider->setMaximum(150);
        txSlider->setOrientation(Qt::Horizontal);
        tySlider = new QSlider(groupBox);
        tySlider->setObjectName(QStringLiteral("tySlider"));
        tySlider->setGeometry(QRect(40, 50, 191, 22));
        tySlider->setMinimum(-150);
        tySlider->setMaximum(150);
        tySlider->setOrientation(Qt::Horizontal);
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 20, 21, 20));
        label->setAlignment(Qt::AlignCenter);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 50, 21, 20));
        label_3->setAlignment(Qt::AlignCenter);
        txSpinner = new IntFloatSpinBox(groupBox);
        txSpinner->setObjectName(QStringLiteral("txSpinner"));
        txSpinner->setGeometry(QRect(240, 20, 51, 22));
        txSpinner->setDecimals(1);
        txSpinner->setMinimum(-15);
        txSpinner->setMaximum(15);
        txSpinner->setSingleStep(0.1);
        tySpinner = new IntFloatSpinBox(groupBox);
        tySpinner->setObjectName(QStringLiteral("tySpinner"));
        tySpinner->setGeometry(QRect(240, 50, 51, 22));
        tySpinner->setDecimals(1);
        tySpinner->setMinimum(-15);
        tySpinner->setMaximum(15);
        tySpinner->setSingleStep(0.1);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(580, 410, 301, 81));
        sxSlider = new QSlider(groupBox_2);
        sxSlider->setObjectName(QStringLiteral("sxSlider"));
        sxSlider->setGeometry(QRect(40, 20, 191, 22));
        sxSlider->setMinimum(1);
        sxSlider->setMaximum(150);
        sxSlider->setValue(10);
        sxSlider->setOrientation(Qt::Horizontal);
        sySlider = new QSlider(groupBox_2);
        sySlider->setObjectName(QStringLiteral("sySlider"));
        sySlider->setGeometry(QRect(40, 50, 191, 22));
        sySlider->setMinimum(1);
        sySlider->setMaximum(150);
        sySlider->setValue(10);
        sySlider->setOrientation(Qt::Horizontal);
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 20, 21, 20));
        label_4->setAlignment(Qt::AlignCenter);
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 50, 21, 20));
        label_5->setAlignment(Qt::AlignCenter);
        sySpinner = new IntFloatSpinBox(groupBox_2);
        sySpinner->setObjectName(QStringLiteral("sySpinner"));
        sySpinner->setGeometry(QRect(240, 50, 51, 22));
        sySpinner->setDecimals(1);
        sySpinner->setMinimum(0.1);
        sySpinner->setMaximum(15);
        sySpinner->setSingleStep(0.1);
        sySpinner->setValue(1);
        sxSpinner = new IntFloatSpinBox(groupBox_2);
        sxSpinner->setObjectName(QStringLiteral("sxSpinner"));
        sxSpinner->setGeometry(QRect(240, 20, 51, 22));
        sxSpinner->setDecimals(1);
        sxSpinner->setMinimum(0.1);
        sxSpinner->setMaximum(15);
        sxSpinner->setSingleStep(0.1);
        sxSpinner->setValue(1);
        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(580, 500, 301, 51));
        thetaSpinner = new QSpinBox(groupBox_4);
        thetaSpinner->setObjectName(QStringLiteral("thetaSpinner"));
        thetaSpinner->setGeometry(QRect(240, 20, 51, 22));
        thetaSpinner->setMaximum(359);
        theta_slider = new QSlider(groupBox_4);
        theta_slider->setObjectName(QStringLiteral("theta_slider"));
        theta_slider->setGeometry(QRect(39, 20, 191, 22));
        theta_slider->setMaximum(359);
        theta_slider->setOrientation(Qt::Horizontal);
        label_6 = new QLabel(groupBox_4);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(9, 20, 21, 20));
        label_6->setAlignment(Qt::AlignCenter);
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 650, 871, 61));
        frameSelection = new FrameSlider(groupBox_3);
        frameSelection->setObjectName(QStringLiteral("frameSelection"));
        frameSelection->setEnabled(false);
        frameSelection->setGeometry(QRect(10, 20, 701, 31));
        frameSelection->setMinimum(0);
        frameSelection->setMaximum(1);
        frameSelection->setOrientation(Qt::Horizontal);
        isKeyframeCheckBox = new QCheckBox(groupBox_3);
        isKeyframeCheckBox->setObjectName(QStringLiteral("isKeyframeCheckBox"));
        isKeyframeCheckBox->setEnabled(false);
        isKeyframeCheckBox->setGeometry(QRect(800, 20, 70, 31));
        isKeyframeCheckBox->setChecked(true);
        frameID = new QLCDNumber(groupBox_3);
        frameID->setObjectName(QStringLiteral("frameID"));
        frameID->setEnabled(false);
        frameID->setGeometry(QRect(720, 10, 64, 41));
        frameSpinner = new FrameCountSelector(centralWidget);
        frameSpinner->setObjectName(QStringLiteral("frameSpinner"));
        frameSpinner->setGeometry(QRect(100, 590, 71, 41));
        frameSpinner->setMinimum(2);
        frameSpinner->setMaximum(300);
        frameSpinner->setValue(2);
        switchAnimateButton = new QPushButton(centralWidget);
        switchAnimateButton->setObjectName(QStringLiteral("switchAnimateButton"));
        switchAnimateButton->setGeometry(QRect(190, 590, 121, 41));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 590, 91, 41));
        clearButton = new QPushButton(centralWidget);
        clearButton->setObjectName(QStringLiteral("clearButton"));
        clearButton->setEnabled(false);
        clearButton->setGeometry(QRect(330, 590, 121, 41));
        resetButton = new QPushButton(centralWidget);
        resetButton->setObjectName(QStringLiteral("resetButton"));
        resetButton->setEnabled(false);
        resetButton->setGeometry(QRect(470, 590, 121, 41));
        playButton = new QPushButton(centralWidget);
        playButton->setObjectName(QStringLiteral("playButton"));
        playButton->setEnabled(false);
        playButton->setGeometry(QRect(610, 590, 121, 41));
        stopButton = new QPushButton(centralWidget);
        stopButton->setObjectName(QStringLiteral("stopButton"));
        stopButton->setEnabled(false);
        stopButton->setGeometry(QRect(750, 590, 121, 41));
        Keyframe2DClass->setCentralWidget(centralWidget);

        retranslateUi(Keyframe2DClass);
        QObject::connect(geometryComboBox, SIGNAL(currentIndexChanged(int)), treeWidget, SLOT(changeGeometrySelection(int)));
        QObject::connect(addChildButton, SIGNAL(clicked()), treeWidget, SLOT(addNewNode()));
        QObject::connect(treeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), treeWidget, SLOT(changeNodeSelection(QTreeWidgetItem*,QTreeWidgetItem*)));
        QObject::connect(treeWidget, SIGNAL(addNode(int)), openGLWidget, SLOT(addNewSGNode(int)));
        QObject::connect(treeWidget, SIGNAL(sendSelectedNode(int)), openGLWidget, SLOT(setSelectedNode(int)));
        QObject::connect(txSlider, SIGNAL(valueChanged(int)), txSpinner, SLOT(valueToConvert(int)));
        QObject::connect(txSpinner, SIGNAL(valueChanged(double)), txSpinner, SLOT(catchChangedValue(double)));
        QObject::connect(txSpinner, SIGNAL(newChanged(int)), txSlider, SLOT(setValue(int)));
        QObject::connect(tySlider, SIGNAL(valueChanged(int)), tySpinner, SLOT(valueToConvert(int)));
        QObject::connect(tySpinner, SIGNAL(newChanged(int)), tySlider, SLOT(setValue(int)));
        QObject::connect(tySpinner, SIGNAL(valueChanged(double)), tySpinner, SLOT(catchChangedValue(double)));
        QObject::connect(sxSlider, SIGNAL(valueChanged(int)), sxSpinner, SLOT(valueToConvert(int)));
        QObject::connect(sxSpinner, SIGNAL(newChanged(int)), sxSlider, SLOT(setValue(int)));
        QObject::connect(sxSpinner, SIGNAL(valueChanged(double)), sxSpinner, SLOT(catchChangedValue(double)));
        QObject::connect(sySlider, SIGNAL(valueChanged(int)), sySpinner, SLOT(valueToConvert(int)));
        QObject::connect(sySpinner, SIGNAL(newChanged(int)), sySlider, SLOT(setValue(int)));
        QObject::connect(sySpinner, SIGNAL(valueChanged(double)), sySpinner, SLOT(catchChangedValue(double)));
        QObject::connect(theta_slider, SIGNAL(valueChanged(int)), thetaSpinner, SLOT(setValue(int)));
        QObject::connect(thetaSpinner, SIGNAL(valueChanged(int)), theta_slider, SLOT(setValue(int)));
        QObject::connect(txSpinner, SIGNAL(valueChanged(double)), openGLWidget, SLOT(setTransX(double)));
        QObject::connect(openGLWidget, SIGNAL(sendTransX(double)), txSpinner, SLOT(setValue(double)));
        QObject::connect(tySpinner, SIGNAL(valueChanged(double)), openGLWidget, SLOT(setTransY(double)));
        QObject::connect(openGLWidget, SIGNAL(sendTransY(double)), tySpinner, SLOT(setValue(double)));
        QObject::connect(sxSpinner, SIGNAL(valueChanged(double)), openGLWidget, SLOT(setScaleX(double)));
        QObject::connect(openGLWidget, SIGNAL(sendScaleX(double)), sxSpinner, SLOT(setValue(double)));
        QObject::connect(sySpinner, SIGNAL(valueChanged(double)), openGLWidget, SLOT(setScaleY(double)));
        QObject::connect(openGLWidget, SIGNAL(sendScaleY(double)), sySpinner, SLOT(setValue(double)));
        QObject::connect(thetaSpinner, SIGNAL(valueChanged(int)), openGLWidget, SLOT(setTheta(int)));
        QObject::connect(openGLWidget, SIGNAL(sendTheta(int)), thetaSpinner, SLOT(setValue(int)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), switchAnimateButton, SLOT(setEnabled(bool)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), addChildButton, SLOT(setEnabled(bool)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), geometryComboBox, SLOT(setEnabled(bool)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), clearButton, SLOT(setDisabled(bool)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), resetButton, SLOT(setDisabled(bool)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), playButton, SLOT(setDisabled(bool)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), stopButton, SLOT(setDisabled(bool)));
        QObject::connect(frameSelection, SIGNAL(valueChanged(int)), frameID, SLOT(display(int)));
        QObject::connect(frameSpinner, SIGNAL(sendFrameCount(int)), frameSelection, SLOT(setFrameCount(int)));
        QObject::connect(frameSpinner, SIGNAL(sendFrameCount(int)), openGLWidget, SLOT(setFrameCount(int)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked()), frameSpinner, SLOT(setCount()));
        QObject::connect(resetButton, SIGNAL(clicked()), frameSpinner, SLOT(setCount()));
        QObject::connect(clearButton, SIGNAL(clicked()), openGLWidget, SLOT(clearKeyFrames()));
        QObject::connect(playButton, SIGNAL(clicked()), openGLWidget, SLOT(startAnimation()));
        QObject::connect(stopButton, SIGNAL(clicked()), openGLWidget, SLOT(stopAnimation()));
        QObject::connect(isKeyframeCheckBox, SIGNAL(clicked(bool)), openGLWidget, SLOT(toggleKey(bool)));
        QObject::connect(openGLWidget, SIGNAL(isKeyFrame(bool)), isKeyframeCheckBox, SLOT(setChecked(bool)));
        QObject::connect(openGLWidget, SIGNAL(changeFrame(int)), frameSelection, SLOT(setValue(int)));
        QObject::connect(frameSelection, SIGNAL(valueChanged(int)), openGLWidget, SLOT(selectFrame(int)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), frameSelection, SLOT(setDisabled(bool)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), frameID, SLOT(setDisabled(bool)));
        QObject::connect(switchAnimateButton, SIGNAL(clicked(bool)), isKeyframeCheckBox, SLOT(setDisabled(bool)));
        QObject::connect(isKeyframeCheckBox, SIGNAL(toggled(bool)), groupBox, SLOT(setEnabled(bool)));
        QObject::connect(isKeyframeCheckBox, SIGNAL(toggled(bool)), groupBox_2, SLOT(setEnabled(bool)));
        QObject::connect(isKeyframeCheckBox, SIGNAL(toggled(bool)), groupBox_4, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(Keyframe2DClass);
    } // setupUi

    void retranslateUi(QMainWindow *Keyframe2DClass)
    {
        Keyframe2DClass->setWindowTitle(QApplication::translate("Keyframe2DClass", "Keyframe2D", 0));
        actionExit->setText(QApplication::translate("Keyframe2DClass", "Exit", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("Keyframe2DClass", "Scene Graph Nodes", 0));
        addChildButton->setText(QApplication::translate("Keyframe2DClass", "Add Child", 0));
        geometryComboBox->clear();
        geometryComboBox->insertItems(0, QStringList()
         << QApplication::translate("Keyframe2DClass", "No Geometry", 0)
         << QApplication::translate("Keyframe2DClass", "Triangle", 0)
         << QApplication::translate("Keyframe2DClass", "Octagon", 0)
         << QApplication::translate("Keyframe2DClass", "Trapezoid", 0)
         << QApplication::translate("Keyframe2DClass", "Square", 0)
         << QApplication::translate("Keyframe2DClass", "Nonconvex", 0)
        );
        groupBox->setTitle(QApplication::translate("Keyframe2DClass", "Translation", 0));
        label->setText(QApplication::translate("Keyframe2DClass", "X", 0));
        label_3->setText(QApplication::translate("Keyframe2DClass", "Y", 0));
        groupBox_2->setTitle(QApplication::translate("Keyframe2DClass", "Scale", 0));
        label_4->setText(QApplication::translate("Keyframe2DClass", "X", 0));
        label_5->setText(QApplication::translate("Keyframe2DClass", "Y", 0));
        groupBox_4->setTitle(QApplication::translate("Keyframe2DClass", "GroupBox", 0));
        label_6->setText(QApplication::translate("Keyframe2DClass", "\316\270", 0));
        groupBox_3->setTitle(QApplication::translate("Keyframe2DClass", "Frame Selection", 0));
        isKeyframeCheckBox->setText(QApplication::translate("Keyframe2DClass", "Keyframe", 0));
        switchAnimateButton->setText(QApplication::translate("Keyframe2DClass", "Switch to Animating", 0));
        label_2->setText(QApplication::translate("Keyframe2DClass", "Frame Count", 0));
        clearButton->setText(QApplication::translate("Keyframe2DClass", "Clear Keyframes", 0));
        resetButton->setText(QApplication::translate("Keyframe2DClass", "Reset Frame Count", 0));
        playButton->setText(QApplication::translate("Keyframe2DClass", "Play", 0));
        stopButton->setText(QApplication::translate("Keyframe2DClass", "Stop", 0));
    } // retranslateUi

};

namespace Ui {
    class Keyframe2DClass: public Ui_Keyframe2DClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYFRAME2D_H
