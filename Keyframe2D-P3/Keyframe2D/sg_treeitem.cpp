#include "sg_treeitem.h"

SG_TreeItem::SG_TreeItem(int number, int type) : QTreeWidgetItem(type) {
	ID = number;
}

SG_TreeItem::~SG_TreeItem() {

}

int SG_TreeItem::getID() {
	return ID;
}
