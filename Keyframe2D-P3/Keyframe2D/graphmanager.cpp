/**
  Need to add comments.
*/

#include "graphmanager.h"
#include "sg_treeitem.h"

GraphManager::GraphManager(QWidget* parent)	: QTreeWidget(parent) {
	setColumnCount(1);
	currentGeometrySelection = 0;
	SG_TreeItem* root = new SG_TreeItem(0);
	root->setText(0, "Root");
	addTopLevelItem(root);
}

GraphManager::~GraphManager() {

}

void GraphManager::changeGeometrySelection(int geomType) {
	currentGeometrySelection = geomType;
}

void GraphManager::addNewNode() {
	static int currentID = 0;
	currentID++;
	SG_TreeItem* newItem = new SG_TreeItem(currentID);
	
	switch(currentGeometrySelection) {
	case 0:
		newItem->setText(0, "No Geometry");
		break;
	case 1:
		newItem->setText(0, "Triangle");
		break;
	case 2:
		newItem->setText(0, "Octagon");
		break;
	case 3:
		newItem->setText(0, "Trapezoid");
		break;
	case 4:
		newItem->setText(0, "Square");
		break;
	case 5:
		newItem->setText(0, "Nonconvex");
		break;
	}
	currentItem()->addChild(newItem);
	
	emit addNode(currentGeometrySelection);
}

void GraphManager::changeNodeSelection(QTreeWidgetItem* current, QTreeWidgetItem* previous) {
	emit sendSelectedNode(static_cast<SG_TreeItem*>(current)->getID());
}