#include "my_openglwidget.h"
#include <algorithm>
using namespace std;

My_OpenGLWidget::My_OpenGLWidget(QWidget* parent) : QOpenGLWidget(parent) {
	vertexShader = nullptr;
	fragmentShader = nullptr;
	shaderProgram = nullptr;
	sgRoot = nullptr;
	sgCurrentNode = nullptr;
	tester = new TestRender(50000);
	tri = new triangle(3);
	quad = new square(4);
	trap = new trapezoid(4);
	oct = new octagon(8);
	chev = new non_convex(18);
	
	//keyframe management
	keyframes.insert(0);
	currentFrame = 0;
	
	//set the framerate
	timer.setInterval(33);
	connect(&timer, &QTimer::timeout, this, &My_OpenGLWidget::incrementFrame);
}

My_OpenGLWidget::~My_OpenGLWidget() {
	delete vertexShader;
	delete fragmentShader;
	delete shaderProgram;
	delete sgRoot;
	delete tester;
	delete tri;
	delete quad;
	delete trap;
	delete oct;
	delete chev;
}

void My_OpenGLWidget::initializeGL() {
	initializeOpenGLFunctions();
	Geometry::context = this;		//let the Geometry nodes render based on this context

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	//skipping the depth testing since we are using 2D graphics, all the depths would be the same
	//may need to have scene graph nodes render after their recursive call

	//allocate shaders and shader program
	vertexShader = new QOpenGLShader(QOpenGLShader::Vertex, this);
	fragmentShader = new QOpenGLShader(QOpenGLShader::Fragment, this);
	shaderProgram = new QOpenGLShaderProgram(this);

	//compile and link the shaders
	vertexShader->compileSourceFile("simple_xform_color.vert");
	fragmentShader->compileSourceFile("pass_through.frag");
	shaderProgram->addShader(vertexShader);
	shaderProgram->addShader(fragmentShader);
	shaderProgram->link();
	
	//set the shader program for use
	shaderProgram->bind();

	//get attribute and uniform locations
	Geometry::vLocation = shaderProgram->attributeLocation("vs_position");
	Geometry::cLocation = shaderProgram->attributeLocation("vs_color");
	Geometry::xformMatrixLocation = shaderProgram->uniformLocation("u_xform");

	//view matrix is handled special, not by geometry
	viewMatrixLocation = shaderProgram->uniformLocation("u_view");

	glGenBuffers(1, &Geometry::vbo);
	glGenBuffers(1, &Geometry::cbo);

	//we are setting up our root node, which will have no geometry drawn for it
	//this way we have a global-level transformation
	sgRoot = new SceneGraph(nullptr);
	sgCurrentNode = sgRoot;	//initial selection is, in fact, the root node
	//note theere is no longer a buildGraph function
}

void My_OpenGLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT);

	//traverse scene graph!
	if(!currentFrame) {
		sgRoot->traverse(gMatrix4::identity());
	}
	else {
		sgRoot->traverse(gMatrix4::identity(), currentFrame);
	}

	glFlush();
}

void My_OpenGLWidget::resizeGL(int width, int height) {
	//sets the window into the rendering context
	glViewport(0, 0, width, height);
	//hardcoded orthographic projection
	gMatrix4 ortho(gVector4(1.0f / 10.0f, 0.0f, 0.0f, 0.0f),
				   gVector4(0.0f, 1.0f / 10.0f, 0.0f, 0.0f),
				   gVector4(0.0f, 0.0f, 1.0f, 0.0f),
				   gVector4(0.0f, 0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(viewMatrixLocation, 1, GL_TRUE, reinterpret_cast<float*>(&ortho));
}

void My_OpenGLWidget::setSelectedNode(int ID) {
	sgCurrentNode = sgRoot->findByID(ID);
	if(keyframes.find(currentFrame) != keyframes.end()) {
		emit sendTransX(static_cast<double>(sgCurrentNode->getTranslateX(currentFrame)));
		emit sendTransY(static_cast<double>(sgCurrentNode->getTranslateY(currentFrame)));
		emit sendScaleX(static_cast<double>(sgCurrentNode->getScaleX(currentFrame)));
		emit sendScaleY(static_cast<double>(sgCurrentNode->getScaleY(currentFrame)));
		emit sendTheta(static_cast<int>(sgCurrentNode->getTheta(currentFrame)));
	}
}

void My_OpenGLWidget::addNewSGNode(int geomType) {
	makeCurrent();

	Geometry* node;
	switch (geomType){
		case 0:
			node = nullptr;
			break;
		case 1:
			node = tri;
			break;
		case 2:
			node = oct;
			break;
		case 3:
			node = trap;
			break;
		case 4:
			node = quad;
			break;
		case 5:
			node = chev;
			break;
	}
	SceneGraph* newSG = new SceneGraph(node);
	sgCurrentNode->addChild(*newSG);

	repaint();
}

void My_OpenGLWidget::setTransX(double value) {
	makeCurrent();
	sgCurrentNode->setTranslateX(currentFrame, static_cast<float>(value));
	repaint();
}

void My_OpenGLWidget::setTransY(double value) {
	makeCurrent();
	sgCurrentNode->setTranslateY(currentFrame, static_cast<float>(value));
	repaint();
}

void My_OpenGLWidget::setScaleX(double value) {
	makeCurrent();
	sgCurrentNode->setScaleX(currentFrame, static_cast<float>(value));
	repaint();
}

void My_OpenGLWidget::setScaleY(double value) {
	makeCurrent();
	sgCurrentNode->setScaleY(currentFrame, static_cast<float>(value));
	repaint();
}

void My_OpenGLWidget::setTheta(int value) {
	makeCurrent();
	sgCurrentNode->setTheta(currentFrame, static_cast<float>(value));
	repaint();
}

void My_OpenGLWidget::selectFrame(int frameNum) {
	makeCurrent();
	currentFrame = frameNum;
	if(keyframes.find(currentFrame) != keyframes.end()) {
		emit sendTransX(static_cast<double>(sgCurrentNode->getTranslateX(currentFrame)));
		emit sendTransY(static_cast<double>(sgCurrentNode->getTranslateY(currentFrame)));
		emit sendScaleX(static_cast<double>(sgCurrentNode->getScaleX(currentFrame)));
		emit sendScaleY(static_cast<double>(sgCurrentNode->getScaleY(currentFrame)));
		emit sendTheta(static_cast<int>(sgCurrentNode->getTheta(currentFrame)));
		emit isKeyFrame(true);
	}
	else {
		emit isKeyFrame(false);
	}
	repaint();
}

void My_OpenGLWidget::incrementFrame() {
	if(currentFrame + 1 == frameTotal - 1) {
		stopAnimation();
	}
	emit changeFrame(currentFrame + 1);
}

void My_OpenGLWidget::startAnimation() {
	emit changeFrame(0);
	timer.start();
}

void My_OpenGLWidget::stopAnimation() {
	timer.stop();
}

void My_OpenGLWidget::clearKeyFrames() {
	setFrameCount(frameTotal - 1);
}

void My_OpenGLWidget::toggleKey(bool state) {
	//check that currentFrame isn't first or last, defend the checkbox's honor
	if(currentFrame == 0 || currentFrame == *(keyframes.crbegin())) {
		emit isKeyFrame(true);
		return;
	}
	if(state) {
		keyframes.insert(currentFrame);
		sgRoot->addKeyframe(currentFrame);
	}
	else {
		keyframes.erase(currentFrame);
		sgRoot->removeKeyframe(currentFrame);
	}
}

void My_OpenGLWidget::setFrameCount(int count) {
	frameTotal = count+1;
	keyframes.clear();
	keyframes.insert(0);
	keyframes.insert(count);
	sgRoot->resetKeyframes(count);
	currentFrame = 0;
}