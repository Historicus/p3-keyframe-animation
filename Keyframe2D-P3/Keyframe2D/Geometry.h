#pragma once

#include "gMatrix4.h"
#include <QOpenGLFunctions>

class Geometry {
	friend class My_OpenGLWidget;
public:
	Geometry(void);
	virtual ~Geometry(void);
	virtual void draw(const gMatrix4&) = 0;

protected:
	static unsigned int vLocation;
	static unsigned int cLocation;
	static unsigned int xformMatrixLocation;
	static unsigned int vbo;
	static unsigned int cbo;
	static QOpenGLFunctions* context;
};