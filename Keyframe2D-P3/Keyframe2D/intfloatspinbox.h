#pragma once

#include <QDoubleSpinBox>

class IntFloatSpinBox : public QDoubleSpinBox {
	Q_OBJECT

public:
	IntFloatSpinBox(QWidget* parent = nullptr);
	~IntFloatSpinBox();

public slots:
	void valueToConvert(int);
	void catchChangedValue(double);

signals:
	void newChanged(int);

private:
	
};