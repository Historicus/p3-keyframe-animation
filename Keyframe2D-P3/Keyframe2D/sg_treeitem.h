#pragma once

#include <QTreeWidgetItem>

class SG_TreeItem : public QTreeWidgetItem {
public:
	SG_TreeItem(int number, int type = Type);
	~SG_TreeItem(void);
	int getID(void);
private:
	int ID;
};
