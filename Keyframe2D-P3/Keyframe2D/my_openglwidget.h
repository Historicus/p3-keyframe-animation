#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLShader>
#include "gVector4.h"
#include "gMatrix4.h"
#include "TestRender.h"
#include "SceneGraph.h"
#include "Triangle.h"
#include "Octagon.h"
#include "non_convex.h"
#include "Square.h"
#include "Trapezoid.h"
#include <set>
#include <QTimer>

class My_OpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	My_OpenGLWidget(QWidget* parent = nullptr);
	~My_OpenGLWidget(void);

	void initializeGL(void);
	void paintGL(void);
	void resizeGL(int, int);

public slots:
	void setSelectedNode(int);
	void addNewSGNode(int);
	void setTransX(double);
	void setTransY(double);
	void setScaleX(double);
	void setScaleY(double);
	void setTheta(int);
	void selectFrame(int);
	void startAnimation(void);
	void stopAnimation(void);
	void clearKeyFrames(void);
	void toggleKey(bool);
	void setFrameCount(int);
	void incrementFrame(void);

signals:
	void sendTransX(double);
	void sendTransY(double);
	void sendScaleX(double);
	void sendScaleY(double);
	void sendTheta(int);
	void changeFrame(int);
	void isKeyFrame(bool);

private:
	QOpenGLShader* vertexShader;
	QOpenGLShader* fragmentShader;
	QOpenGLShaderProgram* shaderProgram;
	unsigned int viewMatrixLocation;
	SceneGraph* sgRoot;
	SceneGraph* sgCurrentNode;
	
	//just to check the rendering context has been set correctly
	TestRender* tester;
	triangle* tri;
	square* quad;
	trapezoid* trap;
	octagon* oct;
	non_convex* chev;

	//frame management
	int currentFrame;
	int frameTotal;
	std::set<int> keyframes;
	QTimer timer;
};