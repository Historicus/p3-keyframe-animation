#include "framecountselector.h"

FrameCountSelector::FrameCountSelector(QWidget* parent)	: QSpinBox(parent) {

}

FrameCountSelector::~FrameCountSelector() {

}

void FrameCountSelector::setCount() {
	emit sendFrameCount(value()-1);
}