#include "intfloatspinbox.h"

IntFloatSpinBox::IntFloatSpinBox(QWidget* parent) : QDoubleSpinBox(parent) {

}

IntFloatSpinBox::~IntFloatSpinBox() {

}

void IntFloatSpinBox::valueToConvert(int value) {
	this->setValue(value / 10.0);
}

void IntFloatSpinBox::catchChangedValue(double value) {
	emit newChanged(static_cast<int>(value * 10.0));
}
