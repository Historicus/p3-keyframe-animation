#pragma once

#include "Geometry.h"
#include "gVector4.h"
#include "gMatrix4.h"

class TestRender : public Geometry {
public:
	TestRender(unsigned int);
	~TestRender(void);
	void draw(const gMatrix4&);

private:
	gVector4* points;
	gVector4* colors;
	unsigned int pointCount;
};

