#pragma once

#include <QSpinBox>

class FrameCountSelector : public QSpinBox {
	Q_OBJECT

public:
	FrameCountSelector(QWidget* parent = nullptr);
	~FrameCountSelector();

public slots:
	void setCount(void);

signals:
	void sendFrameCount(int);

private:
	
};