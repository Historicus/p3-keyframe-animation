#include "frameslider.h"

FrameSlider::FrameSlider(QWidget* parent) : QSlider(parent) {

}

FrameSlider::~FrameSlider() {

}

void FrameSlider::setFrameCount(int total) {
	setMaximum(total);
}