#pragma once

#include <list>
#include <vector>
#include <algorithm>
#include "Geometry.h"
#include "gMatrix4.h"

class SceneGraph {
public:
	//initialize a single keyframe during construction
	SceneGraph(Geometry*);
	SceneGraph(const SceneGraph&);
	~SceneGraph(void);
	void addChild(const SceneGraph&);

	//set recentKeyIndex to 0 as part of the normal traversal
	void traverse(gMatrix4) const;

	//must pass in the frame number for animation
	//keyframes should not be interpolated, and should cause recentKeyIndex to increment
	//other frames should be interpolated by using frameData[recentKeyIndex] and frameData[recentKeyIndex+1]
	void traverse(gMatrix4, int) const;

	//add keyframe
	//recursive, create an interpolated struct, add it to frameData at each node, and add the keyFrameNumber
	void addKeyframe(int);

	//remove keyframe
	//recursive, figure out which frame data is relevant and remove it from each node, delete the keyFrameNumber
	void removeKeyframe(int);

	//reset keyframes - give max frame number
	//recursive, remove all but the first and last elements of frameData, copy the first element's contents into the last element's contents
	//clear the keyFrameNumbers and reinsert 0 and the passed in parameter
	void resetKeyframes(int);

	//setters and getters -- now with frame numbers to take into account
	//need to use the frame number to figure out the correct frameData element to modify/return
	float getScaleX(int) const;
	void setScaleX(int, float);
	
	float getScaleY(int) const;
	void setScaleY(int, float);

	float getTheta(int) const;
	void setTheta(int, float);
	
	float getTranslateX(int) const;
	void setTranslateX(int, float);
	
	float getTranslateY(int) const;
	void setTranslateY(int, float);

	SceneGraph* findByID(int);

private:
	static int nodeCount;

	std::list<SceneGraph> children;
	Geometry* geom;
	unsigned int nodeID;

	//added for animation
	std::vector<int> keyframeNumbers;
	mutable int recentKeyIndex;
	mutable int currentKeyIndex;

	//converted this to a struct to make keyframe management easier
	struct transforms {
		float theta;
		float scaleFactor[2];
		float translation[2];
	};

	//where does the SceneGraph store its own struct?
	std::vector<transforms> frameData;
};

